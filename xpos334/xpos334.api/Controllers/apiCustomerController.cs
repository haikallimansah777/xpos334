﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCustomerController(XPOS_334Context _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                       join r in db.TblRoles on c.IdRole equals r.Id
                                       where c.IsDelete == false
                                       select new VMTblCustomer
                                       {
                                           Id = c.Id,
                                           NameCustomer = c.NameCustomer,
                                           Email = c.Email,
                                           Password = c.Password,
                                           Address = c.Address,
                                           Phone = c.Phone,

                                           IdRole = c.IdRole,
                                           RoleName = r.RoleName,

                                           IsDelete = c.IsDelete,
                                           CreateDate = c.CreateDate,
                                       }).ToList();

            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                  join r in db.TblRoles on c.IdRole equals r.Id
                                  where c.IsDelete == false && c.Id == id
                                 select new VMTblCustomer
                                 {
                                     Id = c.Id,
                                     NameCustomer = c.NameCustomer,
                                     Email = c.Email,
                                     Password = c.Password,
                                     Address = c.Address,
                                     Phone = c.Phone,

                                     IdRole = c.IdRole,
                                     RoleName = r.RoleName,

                                     IsDelete = c.IsDelete
                                 }).FirstOrDefault()!;

            return data;

        }

        [HttpGet("CheckByName/{name}/{id}/{idRole}")]
        public bool CheckName(string name, int id, int idRole)
        {
            TblCustomer data = new TblCustomer();
            if (id == 0)
            {
                data = db.TblCustomers.Where(a => a.NameCustomer == name && a.IsDelete == false
                && a.IdRole == idRole).FirstOrDefault()!;
            }
            else
            {
                data = db.TblCustomers.Where(a => a.NameCustomer == name && a.IsDelete == false
                && a.IdRole == idRole && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpGet("GetDataByIdRole/{id}")]
        public List<VMTblCustomer> GetDataByIdRole(int id)
        {
            List<VMTblCustomer> data = (from c in db.TblCustomers
                                        join r in db.TblRoles on c.IdRole equals r.Id
                                        where c.IsDelete == false && c.Id == id
                                        select new VMTblCustomer
                                       {
                                            Id = c.Id,
                                            NameCustomer = c.NameCustomer,
                                            Email = c.Email,
                                            Password = c.Password,
                                            Address = c.Address,
                                            Phone = c.Phone,

                                            IdRole = c.IdRole,
                                            RoleName = r.RoleName
                                        }).ToList();

            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;
            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.NameCustomer = data.NameCustomer;
                dt.Email = data.Email;
                dt.Password = data.Password;
                dt.Address = data.Address;
                dt.Phone = data.Phone;
                dt.IdRole = data.IdRole;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblCustomer dt = db.TblCustomers.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }


    }
}
