﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;
using xpos334.Services;
using xpos334.Models;

namespace xpos334.Controllers
{
    public class CategoryTryController : Controller
    {
        private readonly XPOS_334Context db;
        private readonly CategoryTryService categoryTryService;

        public CategoryTryController(XPOS_334Context _db)
        {
            db = _db;
            this.categoryTryService = new CategoryTryService(db);
        }
        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryTryService.GetAllData();
            return View(dataView);
        }

        [HttpGet]
        public IActionResult Create()
        {
            VMTblCategory dataView = new VMTblCategory();
            return PartialView(dataView);
        }

        [HttpPost]

        public IActionResult Create(VMTblCategory dataView) 
        {
            VMResponse respon = new VMResponse();
            if(ModelState.IsValid)
            {
                respon = categoryTryService.Create(dataView);

                if(respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Edit (VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = categoryTryService.Edit(dataView);

                if (respon.Success) 
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Detail(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            // ViewBag.tbl_category = dataView;
            // kalo pake viewbag di view.detailnya diubah awalnya (value="@Model.NameCategory") menjadi (value="@ViewBag.tbl_category.NameCategory")
            return PartialView(dataView);
        }

        public IActionResult Delete(int id) 
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Delete(VMTblCategory dataView)
        {
            VMResponse respon = categoryTryService.Delete(dataView);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }
            respon.Entity = dataView;
            return View(respon.Entity);
        }
    }
}
